#!/bin/sh

hasDupes() {
	x=$#
	file=$1
	tmpf=$(eval echo '$'$x)
	returnStr=$file
	
	while [ "$x" -gt "1" ]
	do
		diff "$file" "$tmpf" 1>/dev/null
		if [ "$?" -eq "0" ]
		then
			returnStr=$(echo "$returnStr:$tmpf")
		fi
		x=$(( $x - 1 ))
		tmpf=$(eval echo '$'$x)
	done
	echo $returnStr
	return 0
}

noInput() {
	if [ "$(ls | wc -w)" -eq "lt" 2]
	then
		echo "Usage: compares at least two files to check if there the same.
		There must be at least two files in the directory"
		exit 3
	fi
	
	hasDupes ls
}

[ "$#" -gt "0" ] || noInput

hasDupes $@ 